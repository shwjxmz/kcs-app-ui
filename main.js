import Vue from 'vue'
import App from './App'
import cuCustom from 'colorui/components/cu-custom.vue'
Vue.component('cu-custom',cuCustom)
Vue.config.productionTip = false

//基本全局变量
Vue.prototype.baseURL = function(){
	return 'http://192.168.15.67:9006/';
}
//引入自定义组件
import Page from './Page'
Vue.component('page', Page)

App.mpType = 'app'

const app = new Vue({
    ...App
})

app.$mount()


